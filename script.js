// ==UserScript==
// @name         Report Button
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Report shortcut button on main menu
// @author       You
// @match        https://jira.constellation.soprasteria.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=soprasteria.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    let now = (new Date()).toISOString().split('T')[0];
    let before = (new Date((new Date()).setDate(((new Date()).getDate() -6)))).toISOString().split('T')[0];
    let url = `https://jira.constellation.soprasteria.com/secure/TimesheetReport.jspa?reportKey=jira-timesheet-plugin%3Areport&selectedProjectId=49800&startDate=${before}&endDate=${now}&targetUser=lwestbury&projectRoleId=&projectid=49800&filterid=&priority=&commentfirstword=&sum=&groupByField=&sortBy=&sortDir=ASC&Next=Next`
    let reportButton = document.createElement('a');
    reportButton.innerText = "report";
    reportButton.classList.add("aui-nav-link");
    reportButton.setAttribute('href', url);
    reportButton.setAttribute('target', "_blank");
    let li = document.createElement('li');
    li.insertBefore(reportButton, li.firstChild);

    let nav_secondary_ul = document.getElementsByTagName('nav')[0].firstElementChild.lastElementChild.firstElementChild;
    nav_secondary_ul.insertBefore(li, nav_secondary_ul.firstElementChild);

})();
